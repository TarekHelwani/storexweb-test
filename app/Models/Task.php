<?php

namespace App\Models;

use App\Traits\Filter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'title',
        'checklist_id',
        'status'
    ];

    public const STATUS = ['To Do', 'Done'];

    public function checklist()
    {
        return $this->belongsTo(Checklist::class);
    }

    public function scopeFilterStatus($query, $filter)
    {
        if (in_array($filter, self::STATUS)) {
            return $query->where('status', $filter);
        }
        return $query;
    }

    public function scopeFilterSorting($query, $filter)
    {
        if ($filter) {
            return $query->orderBy('created_at', 'ASC');
        }
        return $query;
    }
}
