<?php

namespace App\Http\Controllers;

use App\Models\Checklist;
use App\Models\User;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{
    public function index()
    {
        $checklists = Checklist::all();

        return view('checklists.index', compact('checklists'));
    }

    public function create()
    {
        $users = User::all('id', 'name');

        return view('checklists.create', compact('users'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'name' => 'required',
                'user_id' => 'required'
            ]
        );

        Checklist::create($validatedData);

        return redirect()->route('checklists.index');
    }


    public function show(Checklist $checklist)
    {
        $checklist->load('tasks', 'user');

        return view('checklists.show', compact('checklist'));
    }

    public function edit(Checklist $checklist)
    {
        $users = User::all('id', 'name');

        return view('checklists.edit', compact('checklist', 'users'));
    }

    public function update(Request $request, Checklist $checklist)
    {
        $validatedData = $request->validate(
            [
                'name' => 'required'
            ]
        );

        $checklist->update($validatedData);

        return redirect()->route('checklists.index');
    }

    public function destroy(Checklist $checklist)
    {
        $checklist->delete();

        return redirect()->route('checklists.index');
    }
}
