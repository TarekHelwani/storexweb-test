<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Models\Checklist;
use App\Models\Task;
use App\Models\User;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::with(['checklist'])
            ->FilterStatus(request('status'))
            ->FilterSorting(request('sort'))
            ->get();

        return view('tasks.index', compact('tasks'));
    }

    public function create()
    {
        $checklists = Checklist::all('id', 'name');

        return view('tasks.create', compact('checklists'));
    }

    public function store(TaskRequest $request)
    {
        Task::create($request->validated());

        return redirect()->route('tasks.index');
    }

    public function show(Task $task)
    {
        return view('tasks.show', compact('task'));
    }

    public function edit(Task $task)
    {
        $checklists = Checklist::all('id', 'name');

        return view('tasks.edit', compact('task', 'checklists'));
    }

    public function update(TaskRequest $request, Task $task)
    {
        $task->update($request->validated());

        return redirect()->route('tasks.index');
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return redirect()->route('tasks.index');
    }
}
