@extends('dashboard.layout')

@section('content')
    <div class="row">
        <div class="col-md-8 mt-4">
            <div class="card border-top-primary">
                <div
                        class="card-header"
                        style="background-color: #FEFCFB;"
                >
                    {{ $task->title }}
                </div>

                <div
                        class="card-footer"
                        style="background-color: #FEFCFB;"
                >
                    <p class="mb-0">Created at {{ $task->created_at->format('M d, Y H:m') }}</p>
                    <p class="mb-0">Updated at {{ $task->updated_at->format('M d, Y H:m') }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-4 mt-4">
            <div class="card border-top-primary">
                <div
                        class="card-header"
                        style="background-color: #FEFCFB;"
                >Information
                </div>

                <div class="card-body">
                    <p class="mb-0">Status {{ ucfirst($task->status) }}</p>
                </div>
            </div>
        </div>


    </div>
@endsection