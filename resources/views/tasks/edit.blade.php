@extends('dashboard.layout')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <form action="{{ route('tasks.update', $task) }}" method="POST">
                @csrf
                @method('PUT')

                <div class="card">
                    <div class="card-header">
                        Edit task
                    </div>

                    <div class="card-body">
                        <x-input-field name="title" label="Title" oldValue="{{ $task->title }}"/>

                        <div class="form-group mt-2">
                            <label for="checklist_id">Assigned Checklist</label>
                            <select class="form-control mt-2 {{ $errors->has('checklist_id') ? 'is-invalid' : '' }}"
                                    name="checklist_id" id="checklist_id" required>
                                @foreach($checklists as $checklist)
                                    <option
                                            value="{{ $checklist->id }}" {{ (old('checklist_id') ? old('checklist_id') : $checklist->id ?? '') == $checklist->id ? 'selected' : '' }}>{{ $checklist->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('checklist_id'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('checklist_id') }}
                                </div>
                            @endif
                            <span class="help-block"> </span>
                        </div>

                        <div class="form-group mt-2">
                            <label for="status">Status</label>
                            <select class="form-control mt-2 {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status"
                                    id="status" required>
                                @foreach(App\Models\Task::STATUS as $status)
                                    <option
                                            value="{{ $status }}" {{ (old('status') ? old('status') : $task->status ?? '') == $status ? 'selected' : '' }}>{{ ucfirst($status) }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('status'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            @endif
                            <span class="help-block"> </span>
                        </div>

                        <button class="btn btn-primary mt-4" type="submit">
                            Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
