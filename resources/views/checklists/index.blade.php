@extends('dashboard.layout')

@section('content')
    <div
            style="margin-bottom: 10px"
            class="row"
    >
        <div class="col-lg-12">
            <a
                    href="{{ route('checklists.create') }}"
                    class="btn btn-success text-white"
            >Create Checklist
            </a>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Checklists list
        </div>

        <div class="card-body">
            <table class="table table-responsive-sm table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Assigned to</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($checklists as $checklist)
                    <tr>

                        <td>
                            <a
                                    style="text-decoration: none"
                                    href="{{ route('checklists.show', $checklist) }}"
                            >{{ $checklist->name }}</a>
                        </td>
                        <td>{{ $checklist->user->name }}</td>
                        <td>
                            <a
                                    class="btn btn-sm btn-info text-white"
                                    href="{{ route('checklists.edit', $checklist) }}"
                            >
                                Edit
                            </a>
                        </td>
                        <td>
                            <form
                                    action="{{ route('checklists.destroy', $checklist) }}"
                                    method="POST"
                                    onsubmit="return confirm('Are your sure?');"
                                    style="display: inline-block;"
                            >
                                @method('DELETE')
                                <input
                                        type="hidden"
                                        name="_method"
                                        value="DELETE"
                                >
                                <input
                                        type="hidden"
                                        name="_token"
                                        value="{{ csrf_token() }}"
                                >
                                <input
                                        type="submit"
                                        class="btn btn-sm btn-danger text-white"
                                        value="Delete"
                                >
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection