@extends('dashboard.layout')

@section('content')
    <div class="row">

        <div class="col-md-6">
            <div class="card">
                <div class="card-header" style="background-color: #FEFCFB;">Assigned user</div>

                <div class="card-body">
                    <p class="mb-0">{{ $checklist->user->name }}</p>
                    <p class="mb-0">{{ $checklist->user->email }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-8 mt-4">
            <div class="card border-top-primary">
                <div class="card-header" style="background-color: #FEFCFB;">
                    {{ $checklist->name }}
                </div>
            </div>
        </div>

        <div class="col-md-4 mt-4 border-top-danger">
            <div class="card border-top-primary">
                <div class="card-header" style="background-color: #FEFCFB;">Information</div>

                <div class="card-body">
                    <p class="mb-0">Created at {{ $checklist->created_at->format('M d, Y') }}</p>
                </div>
            </div>
        </div>

        <div class="col-md-12 mt-4">
            <div class="card border-top-primary">
                <div class="card-header">Tasks</div>
                <div class="card-body">
                    @if($checklist->tasks->count())
                        <table class="table table-sm table-responsive-sm">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Assigned to</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($checklist->tasks as $task)
                                <tr>
                                    <td>
                                        <a style="text-decoration: none" href="{{ route('tasks.show', $task) }}">{{ $task->title }}</a>
                                    </td>
                                    <td>{{ $task->status }}</td>
                                    <td>
                                        <a
                                                class="btn btn-sm btn-info"
                                                href="{{ route('tasks.edit', $task) }}"
                                        >
                                            Edit
                                        </a>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div
                                class="alert alert-info"
                                role="alert"
                        >
                            No tasks added to this project.
                            <a href="{{ route('tasks.create') }}">Create task now.</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection