@extends('dashboard.layout')

@section('content')
    <form action="{{ route('checklists.store') }}" method="POST">
        @csrf

        <div class="card">
            <div class="card-header">
                Create Checklist
            </div>
            <div class="card-body">
                <x-input-field name="name" label="Name"/>

                <div class="form-group mt-2">
                    <label for="user_id">Assigned user</label>
                    <select
                            class="form-control mt-2{{ $errors->has('user_id') ? 'is-invalid' : '' }}"
                            name="user_id"
                            id="user_id"
                            required
                    >
                        @foreach($users as $user)
                            <option value="{{ $user->id }}" {{ (old('user_id') ? old('user_id') : $project->user->id ?? '') == $user->id ? 'selected' : '' }}>{{ $user->name }}</option>
                        @endforeach
                    </select>

                    @if($errors->has('user_id'))
                        <div class="invalid-feedback">
                            {{ $errors->first('user') }}
                        </div>
                    @endif
                    <span class="help-block"> </span>
                </div>

                <button class="btn btn-primary mt-4" type="submit">
                    Save
                </button>
            </div>
        </div>
    </form>
@endsection